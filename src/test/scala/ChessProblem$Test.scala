import org.scalatest.FreeSpec

class ChessProblem$Test extends FreeSpec {
  "Check solution" - {
    "3×3 board containing 2 Kings and 1 Rook" - {
      val solution = ChessProblem.solve(Chess(3, 3), List((Chess.King, 2), (Chess.Rock, 1)))

      assert(solution.size == 4)
      assert(solution.mkString("\n") ==
          "*R*\n" +
          "***\n" +
          "K*K\n" +
          "\n" +
          "**K\n" +
          "R**\n" +
          "**K\n" +
          "\n" +
          "K**\n" +
          "**R\n" +
          "K**\n" +
          "\n" +
          "K*K\n" +
          "***\n" +
          "*R*\n")
    }
  }
}
